<div class="sidebar">
    <div class="widget">
        <h3 class="widget__title"><?php _e("Archive", THEME_TEXT) ?></h3>
        <ul>
    <?php
    global $wpdb;
    $limit = 0;
    $months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,  YEAR( post_date ) AS year, COUNT( id ) as post_count FROM $wpdb->posts WHERE post_status = 'publish' and post_date <= now( ) and post_type = 'post' GROUP BY month , year ORDER BY post_date DESC");
    foreach($months as $month) :
        $class='cat';
        if(get_the_date('Ym')==$month->year.date("m", mktime(0, 0, 0, $month->month, 1, $month->year))){
            $class='cat current-cat';
        }
        ?>
        <li class="<?php echo $class;?>"><a href="<?php bloginfo('url') ?>/<?php echo $month->year; ?>/<?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>"><div style="float: left" class="archive-month"><?php echo date_i18n("F", mktime(0, 0, 0, $month->month, 1, $month->year)); echo ' '.$month->year; ?></div><div style="float: right"><?php //echo $month->post_count;?></div></a><div style="clear: both;"></div> </li>
        <?php
            if(++$limit >= 18) { break; }

    endforeach; ?>
        </ul>
    </div>

    <?php dynamic_sidebar('sidebar'); ?>
</div>