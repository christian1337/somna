<article>
    <?php
    $class='-no-image';
    if(has_post_thumbnail()){
        $class='';
        ?>
    <div class="article-image">
        <a href="<?php echo get_post_permalink()?>">
            <?php the_post_thumbnail('full')?>
        </a>
    </div>
    <?php } ?>
    <?php
    if (isset($_GET['dev'])) {

        var_dump(wp_get_post_categories());
    }
    ?>
    <div class="article-content<?php echo $class; ?>">
        <h1><?php the_title();?></h1>
        <div class="meta"><?php the_date();?><span> <?php $categories = get_the_category();
                foreach($categories as $category){
                    if($category->category_parent==0){
                        $link = get_category_link( $category->term_id );
                        echo sprintf('<a class="news_cat" href="%s">%s</a>', $link, $category->cat_name);
                    }
                } ?></span></div>
        <p><?php the_excerpt();?></p>
        <a href="<?php echo get_post_permalink()?>" class="btn btn-default"><?php _e('Read more', THEME_TEXT)?></a>
    </div>
</article>