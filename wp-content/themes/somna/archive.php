<?php
get_header(); ?>

<div class="container">
    <?php
        $post_type = get_post_type();
        if ( have_posts() ) :
    ?>
        <header class="page-header">
            <h1 class="page-title">
                <?php
                if ( is_day() ) :
                    printf( __( 'Daily Archives: %s', THEME_TEXT ), get_the_date() );
                elseif ( is_month() ) :
                    printf( __( 'Monthly Archives: %s', THEME_TEXT ), get_the_date( _x( 'F Y', 'monthly archives date format', THEME_TEXT ) ) );
                elseif ( is_year() ) :
                    printf( __( 'Yearly Archives: %s', THEME_TEXT ), get_the_date( _x( 'Y', 'yearly archives date format', THEME_TEXT ) ) );
                else :
                    echo get_queried_object()->name;
                    /*switch ($post_type){
                        case 'references':
                            echo get_queried_object()->name;
                            break;
                        default:
                            _e("News", THEME_TEXT);
                            echo get_queried_object()->name;
                    }*/
                endif;
                ?>
            </h1>
        </header><!-- .page-header -->
    <div class="row">
        <div class="articles">
        <?php
        // Start the Loop.
        while ( have_posts() ) : the_post();
            get_template_part('content', 'blog');
        endwhile;
        echo '</div>';
    else :
        get_template_part( 'content', 'none' );
    endif;
    get_sidebar($post_type);
    ?>
        </div>
</div>
<?php
get_footer();