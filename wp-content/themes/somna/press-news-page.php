<?php
/* Template Name: Press & Nyheter */
get_header();?>
<div class="press-news-top">
    <div class="container">
    <?php wp_nav_menu(array(
        'theme_location' => 'press-news-menu',
        'container' => 'nav',
        'container_id' => 'press-news-menu-container',
        'container_class' => 'nav'
    ));  ?>
    </div>
</div>
    <div id="main-content" class="main-content">
        <div class="container wp-content">
            <?php the_content() ?>
        </div>
<?php
get_footer();
