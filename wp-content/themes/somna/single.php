<?php
    get_header();
    $post_type = get_post_type();
?>
<div class="container">
    <div class="row">
    <div class="articles">
    <?php
    if ( have_posts()){
        while ( have_posts() ) : the_post();
        ?>
            <div class="content">
                <h1><?php the_title()?></h1>
                <div class="meta"><?php the_date();?><span> <?php $categories = get_the_category();
                        foreach($categories as $category){
                            if($category->category_parent==0){
                                $link = get_category_link( $category->term_id );
                                echo sprintf('<a class="news_cat" href="%s">%s</a>', $link, $category->cat_name);
                            }
                        } ?></span></div>
                <p><?php the_content();?></p>
            </div>
        <?php
        endwhile;
    }
    ?>
    </div>
    <?php get_sidebar($post_type);?>
    </div>
</div>
<?php get_footer();?>