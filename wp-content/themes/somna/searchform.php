<form method="get" id="searchform" autocomplete="off" action="<?php bloginfo('url'); ?>/">
	<label class="hidden" for="s"><?php _e(''); ?></label>
	<input type="text" value="<?php the_search_query(); ?>" name="s" placeholder="<?php _e('Search', THEME_TEXT)?>" class="search__field"/>
    <button type="submit" class="search__btn">
        <i aria-hidden="true" class="ei ei-search2"></i>
    </button>
</form>