<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108099344-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-108099344-1');
    </script>
    <title><?php wp_title( '|', true, 'right' );?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/images/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/images/favicon-16x16.png" sizes="16x16" />

    <?php
    /*global $themeStyle, $themeScript;
    $themeStyle = new WordPressStyle(get_template_directory_uri().'/css/style.css?ver=1.1');
    $themeScript = new WordPressScript(get_template_directory_uri().'/js/theme.js?ver=1.1');*/

    new WordPressGoogleFont(array(
        'Lato' => array('normal', 'bold'),
        'Muli' => array('light', 'normal', 'bold')
    ));

    wp_head();
    ?>
</head>
<body <?php body_class(ICL_LANGUAGE_CODE); ?> data-language="<?php echo ICL_LANGUAGE_CODE; ?>">
<header>
    <div id="header">
        <div id="info"></div>
        <div id="header-top" class="container-fluid">
            <div class="container">
                <div class="flag-menu">
                    <?php do_action('icl_language_selector'); ?>
                </div>
                <div class="user-menu">
                    <?php
                    if (ICL_LANGUAGE_CODE == 'sv') {
                       ?>
                        <a href="http://sleepie.se">För privatpersoner</a>
                    <?php
                    }else{
                        ?>
                        <a href="<?php the_field('top_menu_button_url', 'option') ?>"><?php the_field('top_menu_button', 'option') ?></a>
                    <?php
                    }?>

                </div>
                <div class="newsletter">
                    <span><?php _e('Join our newsletter', THEME_TEXT)?></span>
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                        <form action="//somna.us11.list-manage.com/subscribe/post?u=7da18a37e799cfbfefb751b3d&amp;id=107f2c56e1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <label for="mce-EMAIL"></label>
                                <input type="email" value="" name="EMAIL" class="input-text" id="mce-EMAIL" placeholder="<?php _e('E-mail address', THEME_TEXT)?>" required>
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_7da18a37e799cfbfefb751b3d_107f2c56e1" tabindex="-1" value=""></div>
                                <input type="submit" value="<?php _e('Send', THEME_TEXT);?>" name="subscribe" id="mc-embedded-subscribe" class="button-join">
                            </div>
                        </form>
                    </div>
                    <!--End mc_embed_signup-->
                </div>
                <div class="cart">
                    <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
                        <div class="count"><div class="counter"><?php echo WC()->cart->get_cart_contents_count(); ?></div></div>
                    </a>
                </div>
                <div class="search">
                <?php get_search_form(); ?>
                </div>

            </div>
        </div>
        <div class="container">
            <div id="header-bottom">
                <a href="<?php echo get_home_url(); ?>"><div class="logo"></div></a>
                <div class="menu-container">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'main-menu',
                        'container' => 'nav',
                        'container_id' => 'main-menu-container',
                        'container_class' => 'nav'
                    ));  ?>
                </div>
            </div>
        </div>
        <div id="menu-container-sub">
            <div class="container"><ul><li>&nbsp;</li></ul></div>
        </div>
        <div id="menu-container-sub-sub">
            <div class="container"><ul><?php echo MainProductCat::getMainCatProducts(); ?></ul></div>
        </div>
    </div>
    <div id="breadcrumbs">
        <div class="container">
            <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p>','</p>');} ?>
        </div>
    </div>
    <div id="mobile-header" class="container-fluid">
        <div class="top">
            <a href="<?php echo get_home_url(); ?>"><div class="logo"></div></a>
            <?php do_action('icl_language_selector'); ?>
        </div>
        <div id="mb-menu-toggle-btn"></div>
        <div id="mb-menu-toggle"><div id="mobile-menu">
                <ul class="container"><li><a href="<?php the_field('top_menu_button_url', 'option') ?>"><?php the_field('top_menu_button', 'option') ?></a></li></ul>
                <ul class="content container"></ul>
            </div>
        </div>
    </div>
</header>
<div class="wrapper">