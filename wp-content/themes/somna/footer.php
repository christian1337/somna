<div style="clear:both"></div>
</div> <!-- END .wrapper -->
<footer>
    <?php if (get_field('footer_use_extra_row') || MainProductCat::getPage(false)){ ?>
        <div class="footer extra-row container-fluid no-padding">
            <div class="container padding-fix">
                <div class="col">
                    <?php the_field('footer_extra_text', 'option');?>
                </div>
                <div class="col">
                    <div class="image"><?php echo wp_get_attachment_image(get_field('footer_extra_image', 'option'), 'full');?></div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="footer top container-fluid no-padding">
        <div class="container padding-fix">
            <div class="col">
                <div class="logo"></div>
                <?php echo Encoding::toUTF8(get_field('footer_about', 'option'));?>
            </div>
            <div class="col">
                <h2><?php the_field('footer_image_and_text_links_header', 'option');?></h2>
                <ul>
                    <?php
                    while ( have_rows('footer_image_and_text_links', 'option') ) : the_row();
                        $url = get_sub_field('footer_image_and_text_url');
                        if($url){
                            $url = get_term_link(get_sub_field('footer_image_and_text_url'));
                            echo '<li><div class="image" style="background-image: url('.wp_get_attachment_image_src(get_sub_field('footer_image_and_text_links_image'), 'medium')[0].')"></div><a href="'.$url.'">'.get_sub_field('footer_image_and_text_links_text').'</a></li>';
                        }
                    endwhile;
                    ?>
                </ul>
            </div>
            <div class="col">
                <h2><?php the_field('footer_contact_header', 'option');?></h2>
                <?php echo do_shortcode(get_field('footer_contact', 'option'));?>
            </div>
        </div>
    </div>
    <div class="footer container-fluid">
        <div class="container">
            <span>&copy; Copyright <?php echo date("Y"); ?> <span class="company-info"><?php the_field('footer_company_info', 'option');?></span></span>
            <ul class="social">
                <?php
                while ( have_rows('wc_social_networks', 'option') ) : the_row();
                    echo '<li><a href="'.get_sub_field('wc_social_network_url').'"><i class="fa '.get_sub_field('wc_social_network_icon').'" aria-hidden="true"></i></a></li>';
                endwhile;
                ?>
            </ul>
            <?php wp_nav_menu(array('theme_location' => 'footer-menu', 'container_class' => 'footer-menu'));  ?>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>