<?php
class MainProductCat
{
    public static function getPage($render)
    {

        $_term_id = get_queried_object()->term_id;

        if (!$_term_id) return false;

        $category_post = get_posts(array(
            'numberposts' => 1,
            'post_type' => 'main_product_cat',
            'meta_key' => 'cat-connect',
            'meta_value' => $_term_id
        ));

        if (!$category_post) return false;

        if ($render) {
            setup_postdata($GLOBALS['post'] =& $category_post[0]);
            the_content();
            wp_reset_postdata();
        }

        return true;
    }

    public static function getSlug()
    {
        return get_queried_object()->slug;
    }

    public static function getMainCat()
    {
        global $post;
        $terms = get_the_terms($post->ID, 'product_cat');
        return $terms[0]->slug;
    }

    public static function getMainCatProducts()
    {
        $_term_slug = self::getMainCat();
        if(!$_term_slug){
            $_term_slug = self::getSlug();
        }
        if(!$_term_slug) return;

        $query_args = array(
            'numberposts' => -1,
            'posts_per_page' =>-1,
            'post_type' => 'product',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'terms' => $_term_slug,
                    'field' => 'slug',
                    'include_children' => true,
                    'operator' => 'IN'
                )
            ),
        );
        $_current = get_the_title();
        $query = new WP_Query($query_args);
        $_html = '';
        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();
                $class = '';
                if($_current==get_the_title()){
                    $class = 'current-menu-item';
                }
                $_html .= '<li class="'.$class.'"><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
            endwhile;
        }
        wp_reset_query();
        return $_html;
    }
}