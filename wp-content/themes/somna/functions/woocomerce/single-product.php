<?php

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 1 );

add_action('woocommerce_before_add_to_cart_form', function() {
    $_html = '';
    $_html .= '<div class="purchase-info">';
    $_html .= '<div class="col">';
    $_html .= '<h2>'.__( 'Open purchase', THEME_TEXT ).'</h2>21 ' .__( 'Days', THEME_TEXT );
    $_html .= '</div>';
    $_html .= '<div class="col">';
    if(ICL_LANGUAGE_CODE=='sv' || ICL_LANGUAGE_CODE=='nb'){
        $_html .= '<h2>'.__( 'Delivery cost', THEME_TEXT ).'</h2>0 '. get_woocommerce_currency_symbol();
    } else {
        $_html .= '<h2>'.__( 'Delivery cost', THEME_TEXT ).'</h2>'.get_woocommerce_currency_symbol().'0';
    }
    $_html .= '</div>';
    $_html .= '</div>';
    echo $_html;

}, 1);


add_action( 'woocommerce_after_single_product_summary', function() {
    $_html = '<div class="videos">';
    if( have_rows('videos') ):
        while ( have_rows('videos') ) : the_row();
            $on_lang = get_sub_field('show_on_language');
            if(in_array(ICL_LANGUAGE_CODE, $on_lang)){
                $_html .= '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/'.get_sub_field('youtube_embed_code').'?showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe></div>';
            }
        endwhile;
    endif;
    $_html .= '</div>';
    echo $_html;
}, 9 );
