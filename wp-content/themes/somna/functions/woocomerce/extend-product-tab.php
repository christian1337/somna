<?php
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {

    // Adds the new tab

    $tabs['doctor'] = array(
        'title' 	=> __( 'For prescribers', THEME_TEXT ),
        'priority' 	=> 50,
        'callback' 	=> 'woo_new_product_tab_content'
    );

    $tabs['manual'] = array(
        'title' 	=> __( 'Manual', THEME_TEXT ),
        'priority' 	=> 50,
        'callback' 	=> 'woo_new_product_tab_content'
    );

    return $tabs;

}
function woo_new_product_tab_content($tab) {

    // The new tab content

    switch ($tab){
        case 'doctor':
            echo '<h2>'.__( 'For prescribers', THEME_TEXT ).'</h2>';
            $org_id = apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'sv' );
            if(ICL_LANGUAGE_CODE=='sv'){
                the_field('tab_info_doctor', $org_id);
            }else{
                $field_name = 'tab_info_doctor_' . ICL_LANGUAGE_CODE;
                the_field($field_name, $org_id);
            }
            break;
        case 'manual':
            echo '<h2>'.__( 'Manual', THEME_TEXT ).'</h2>';

            $org_id = apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'sv' );
            if(ICL_LANGUAGE_CODE=='sv'){
                the_field('tab_info_manual');

                if(get_field('tab_info_manual_pdf')) {
                    echo '<div class="pdf"><a href="' . get_field('tab_info_manual_pdf') . '">' . get_field('tab_info_manual_name') . '</a></div>';
                }

                if( have_rows('tab_other_documents') ):
                    while ( have_rows('tab_other_documents') ) : the_row();
                        echo '<div class="pdf"><a href="' . get_sub_field('tab_info_document_file') . '">' . get_sub_field('tab_info_document_name') . '</a></div>';
                    endwhile;
                else :
                    // no rows found
                endif;

            }else{
                $field_name = 'tab_info_manual_' . ICL_LANGUAGE_CODE;
                the_field($field_name, $org_id);
                $field_name_manual_pdf = 'tab_info_manual_pdf_' . ICL_LANGUAGE_CODE;
                $field_name_manual_name = 'tab_info_manual_name_' . ICL_LANGUAGE_CODE;
                if(get_field($field_name_manual_pdf, $org_id)) {
                    echo '<div class="pdf"><a href="' . get_field($field_name_manual_pdf, $org_id) . '">' . get_field($field_name_manual_name, $org_id) . '</a></div>';
                }

                if( have_rows('tab_other_documents_'.ICL_LANGUAGE_CODE, $org_id) ):
                    while ( have_rows('tab_other_documents_'.ICL_LANGUAGE_CODE, $org_id) ) : the_row();
                        echo '<div class="pdf"><a href="' . get_sub_field('tab_info_document_file_'.ICL_LANGUAGE_CODE, $org_id) . '">' . get_sub_field('tab_info_document_name_'.ICL_LANGUAGE_CODE, $org_id) . '</a></div>';
                    endwhile;
                else :
                    // no rows found
                endif;

                the_field($field_name, $org_id);
            }


            break;
    }
}