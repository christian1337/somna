<?php
class VcElementAsset {

    public function __construct() {

    }

    public static function attach_image($param_name, $group='General'){
        return array(
            'type' => 'attach_image',
            'heading' => __( 'Image', 'text-domain' ),
            'param_name' => $param_name,
            'value' => '',
            'description' => __( 'Select image from media library.', 'text-domain' ),
            'admin_label' => false,
            'weight' => 0,
            'group' => $group
        );
    }

    public static function dropdown($param_name, $values, $heading, $group='General', $dependency=[]){
        return array(
            'type' => 'dropdown',
            'class' => 'title-class',
            'heading' => __( $heading, 'text-domain' ),
            'param_name' => $param_name,
            'value' => $values,
            'description' => __( '' ),
            'admin_label' => false,
            'weight' => 0,
            'group' => $group,
            'dependency' => $dependency
        );
    }

    public static function vc_link($param_name='url', $heading='Link to', $group='General', $dependency=[]){
        return array(
            'type' => 'vc_link',
            'heading' => __( $heading, 'text-domain' ),
            'param_name' => $param_name,
            'value' => '',
            'admin_label' => false,
            'weight' => 0,
            'group' => $group,
            'dependency' => $dependency
        );
    }

    public static function textarea_html($heading='Content', $group='General')
    {
       return array(
            'type' => 'textarea_html',
            'heading' => __($heading, 'text-domain'),
            'param_name' => 'content',
            'value' => __(''),
            'description' => __('Text', 'text-domain'),
            'admin_label' => false,
            'weight' => 2,
            'group' => $group
        );
    }
    public static function textarea ($param_name, $heading='Content', $group='General')
    {
        return array(
            'type' => 'textarea',
            'heading' => __($heading, 'text-domain'),
            'param_name' => $param_name,
            'value' => __(''),
            'admin_label' => false,
            'group' => $group
        );
    }
    public static function textfield ($param_name, $heading='Content', $group='General', $value='')
    {
        return array(
            'type' => 'textfield',
            'heading' => __($heading, 'text-domain'),
            'param_name' => $param_name,
            'value' => $value,
            'admin_label' => false,
            'group' => $group,
            'holder' => 'vc_param-group-admin-labels',
        );
    }
    public static function colorpicker ($param_name, $heading, $group='General')
    {
        return array(
            'type' => 'colorpicker',
            'heading' => __($heading, 'text-domain'),
            'param_name' => $param_name,
            'value' => __(''),
            'admin_label' => false,
            'group' => $group,
        );
    }
    public static function checkbox ($param_name, $values, $heading, $group='General')
    {
        return array(
            'type' => 'checkbox',
            'heading' => __($heading, 'text-domain'),
            'param_name' => $param_name,
            'value' => $values,
            'admin_label' => false,
            'group' => $group,
        );
    }
}