<?php
Class Icons
{
    public static function icon($type){
        $name = '';
        switch($type){
            case 'private':
                $name = __('Private', THEME_TEXT);
                break;
            case 'day':
                $name = __('Day', THEME_TEXT);
                break;
            case 'night':
                $name = __('Night', THEME_TEXT);
                break;
            case 'sensitive':
                $name = __('Private', THEME_TEXT);
                break;
            case 'bag':
                $name = __('Wash Bag', THEME_TEXT);
                break;
            case 'pro':
                $name = __('Professionals', THEME_TEXT);
                break;
            case 'twoone':
                $name = __('2-1 Function', THEME_TEXT);
                break;
            case 'zipper':
                $name = __('Zipper', THEME_TEXT);
                break;
            default:
                return '';
                break;
        }
        return '<div class="'.$type.'">'.$name.'</div>';
    }

    public static function icon_content_class($type){
        if($type=='none'){
            return 'no-icon';
        }else{
            return 'yes-icon';
        }
    }
}