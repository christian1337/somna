<?php
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {

    // Adds the new tab

    $tabs['doctor'] = array(
        'title' 	=> __( 'Info till förskrivare', THEME_TEXT ),
        'priority' 	=> 50,
        'callback' 	=> 'woo_new_product_tab_content'
    );

    $tabs['manual'] = array(
        'title' 	=> __( 'Bruksanvsining', THEME_TEXT ),
        'priority' 	=> 50,
        'callback' 	=> 'woo_new_product_tab_content'
    );

    return $tabs;

}
function woo_new_product_tab_content($tab) {

    // The new tab content

    switch ($tab){
        case 'doctor':
            echo '<h2>'.__( 'Info till förskrivare', THEME_TEXT ).'</h2>';
            the_field('tab_info_doctor');
            break;
        case 'manual':
            echo '<h2>'.__( 'Bruksanvsining', THEME_TEXT ).'</h2>';
            the_field('tab_info_manual');
            break;
    }
}