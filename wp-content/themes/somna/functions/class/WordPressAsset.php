<?php
abstract class WordPressAsset {
    protected $_handle = null;
    protected $_src = null;
    protected $_variables = array();
    protected $_code = '';

    public function __construct($src, $vars = array(), $handle = null) {
        $this->_src = $src;

        if ($handle)
            $this->_handle = $handle;
        else
            $this->_handle = substr(md5(rand()), 0, 10);

        if (!empty($vars))
            $this->addVariables($vars);

        $this->enqueueSource();
        add_action('wp_footer', array($this, 'enqueueVariables'));
        add_action('wp_footer', array($this, 'enqueueInline'));
    }

    public function addVariables($vars, $varName = null) {
        if ($varName === null && is_array($vars))
            $this->_variables = self::_arrayMergeOverwrite($this->_variables, $vars);
        else if ($varName !== null && is_array($varName))
            self::_arrayDeepInsert($this->_variables, $varName, $vars);
    }

    public function addInline($code) {
        $this->_code .= $code;
    }

    public abstract function enqueueSource();

    public abstract function enqueueVariables();

    public abstract function enqueueInline();

    protected static function _arrayDeepInsert(&$array, $keys, $value = null) {
        if (!is_array($keys))
            $keys = array($keys);

        if (is_numeric($keys[0]))
            $keys[0] = $keys[0] + 0;

        if (!isset($array[$keys[0]]))
            $array[$keys[0]] = array();

        if (count($keys) > 1)
            self::_arrayDeepInsert($array[$keys[0]], array_slice($keys, 1), $value);
        else
            $array[$keys[0]] = (is_array($value))?self::_arrayMergeOverwrite($array[$keys[0]], $value):$value;
    }

    protected static function _arrayMergeOverwrite($array1, $array2) {
        foreach ($array2 as $k => $v) {
            if (is_array($v) && isset($array1[$k]) && is_array($array1[$k]))
                $array1[$k] = self::_arrayMergeOverwrite($array1[$k], $v);
            else
                $array1[$k] = $v;
        }

        return $array1;
    }
}

class WordPressScript extends WordPressAsset {
    public function addVariables($vars, $varName = null) {
        if ($varName !== null && !is_array($varName))
            $varName = explode('.', $varName);

        if (is_array($vars))
            $vars = self::_parseDots($vars);

        parent::addVariables($vars, $varName);
    }

    public function addInline($code) {
        $this->_code = !empty($this->_code)?rtrim($this->_code, ';').';'.$code:$code;
    }

    public function enqueueSource() {
        wp_enqueue_script($this->_handle, $this->_src, array(), null, true);
    }

    public function enqueueVariables() {
        if (empty($this->_variables))
            return;

        foreach ($this->_variables as $key => $value)
            wp_localize_script($this->_handle, $key, $value);
    }

    public function enqueueInline() {
        if (empty($this->_code))
            return;

        if (function_exists('wp_add_inline_script'))
            wp_add_inline_script($this->_handle, $this->_code);
        else
            printf('<script>%s</script>',  $this->_code);
    }

    private static function _parseDots($vars) {
        foreach ($vars as $key => &$value) {
            if (is_array($value))
                $value = self::_parseDots($value);

            $keys = explode('.', $key);
            if (count($keys) == 1)
                continue;

            unset($vars[$key]);
            self::_arrayDeepInsert($vars, $keys, $value);
        }

        return $vars;
    }
}

class WordPressStyle extends WordPressAsset {
    public function enqueueSource() {
        wp_enqueue_style($this->_handle, $this->_src, array(), null);
    }

    public function enqueueVariables() {
        if (empty($this->_variables))
            return;

        $style = self::_generateStyle($this->_variables, $parent = '');

        self::_printStyle($style);
    }

    public function enqueueInline() {
        if (empty($this->_code))
            return;

        self::_printStyle($this->_code);
    }

    private static function _printStyle($style) {
        printf('<style>%s</style>', $style);
    }

    private static function _generateStyle($array, $parent = '') {
        $props = array();
        $output = '';

        foreach ($array as $key => $value) {
            if (is_array($value))
                $output .= self::_generateStyle($value, trim($parent.' '.$key));
            else
                $props[$key] = $value;
        }

        if (!empty($props)) {
            if (!$parent)
                $parent = '*';

            $output .= $parent.'{';

            foreach ($props as $key => $value) {
                $output .= $key.':'.$value.';';
            }

            $output .= '}';
        }

        return $output;
    }
}

class WordPressGoogleFont extends WordPressStyle {
    public function __construct($fonts) {
        $format = 'https://fonts.googleapis.com/css?family=%s';

        if (!is_array($fonts))
            $fonts = array($fonts);

        $faces = array();

        foreach ($fonts as $k => $v) {
            // Associative, key is font name
            if (is_string($k)) {
                if (!is_array($v))
                    $v = array($v);

                foreach ($v as $k2 => $v2) {
                    if (!is_array($v2))
                        $v2 = array($v2);

                    // Subkeys are weights
                    if (is_string($k2) || self::_isNumericWeight($k2)) {
                        $weight = self::_weight($k2);

                        if ($weight === null)
                            continue;

                        foreach ($v2 as $styleName) {
                            if (($style = self::_style($styleName)) !== null)
                                $faces[$k][] = array($weight, $style);
                        }
                    }
                    // Values are weights/style
                    else {
                        foreach ($v2 as $weightAndStyle) {
                            $data = array(null, null);

                            foreach(explode(' ', $weightAndStyle) as $weightStyleName) {
                                if (($weight = self::_weight($weightStyleName)) !== null && !isset($data[0]))
                                    $data[0] = $weight;

                                else if (($style = self::_style($weightStyleName)) !== null && !isset($data[1]))
                                    $data[1] = $style;
                            }

                            if (!isset($data[0]) && isset($data[1]))
                                $data[0] = self::_weight('normal');

                            if (isset($data[0]) && !isset($data[1]))
                                $data[1] = self::_style('normal');

                            if (isset($data[0]) && isset($data[1]))
                                $faces[$k][] = $data;
                        }
                    }
                }
            }
            // Values are font names
            else
                $faces[$v] = array();
        }

        foreach ($faces as $name => &$weightsAndStyles) {
            $name = urlencode($name);
            if (!empty($weightsAndStyles)) {
                foreach ($weightsAndStyles as &$weightAndStyle)
                    $weightAndStyle = implode('', $weightAndStyle);

                $weightsAndStyles = $name.':'.implode(',', $weightsAndStyles);
            }
            else
                $weightsAndStyles = $name;
        }

        parent::__construct(sprintf($format, implode('|', $faces)));
    }

    private static function _weight($name) {
        switch($name) {
            case 'thin': return 100;
            case 'ultralight': return 200;
            case 'light': return 300;
            case 'normal': return 400;
            case 'regular': return 400;
            case 'medium': return 500;
            case 'semibold': return 600;
            case 'demibold': return 600;
            case 'bold': return 700;
            case 'ultrabold': return 800;
            case 'black': return 900;
        }

        if (self::_isNumericWeight($name))
            return $name;

        return null;
    }

    private static function _isNumericWeight($name) {
        $name = (int)$name;
        return ($name%100 == 0 && $name <= 900 && $name >= 100);
    }

    private static function _style($name) {
        switch($name) {
            case 'normal': return '';
            case 'italic': return 'i';
            case 'oblique': return 'i';
        }

        return null;
    }
}