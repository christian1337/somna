<?php
class AttributeSorting {
    public static function sortAttributes($options, $attributes)
    {
        $sort = [];
        $cc = 0;
        foreach($attributes as $attribute){
            $sort[$attribute->slug] = $cc;
            $cc++;
        }

        usort($options, function ($a, $b) use ($sort) {
            $asize = $sort[$a];
            $bsize = $sort[$b];

            if ($asize == $bsize)
                return 0;

            return ($asize > $bsize) ? 1 : -1;
        });

        return $options;
    }
}