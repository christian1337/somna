<?php
class Ajax {
    public static function init() {
        self::_addAction('wp_ajax_nopriv_data', 'data');
        self::_addAction('wp_ajax_data', 'data');
    }

    public static function data(){
        $call = $_POST['call'];
        $vars = $_POST['vars'];
        switch ($call){
            case 'geolocate':
                //$ip_address = WC_Geolocation::get_external_ip_address( ); // local host testing
                $ip_address = WC_Geolocation::get_ip_address( );
                $geo_location = WC_Geolocation::geolocate_ip( $ip_address, false, false );
                die(json_encode(['country'=>$geo_location['country'], 'cart'=>WC()->cart->get_cart_contents_count(), 'call'=>$call]));
                break;
            case 'calendar':
                $vars = [
                    'call' => 'calendar',
                    'html' => EventCalendar::getMonth($vars['year'],$vars['month'],$vars['day']),
                    'year' => $vars['year'],
                    'month' => $vars['month'],
                    'day' => $vars['day']
                ];
                die(json_encode($vars));
                break;
        }
    }

    protected static function _addAction($hook, $method, $priority = 10, $accepted_args = 1) {
        add_Action($hook, array(__CLASS__, $method), $priority, $accepted_args);
    }
}

Ajax::init();