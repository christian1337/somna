<?php
class DummyContent {
    /*
     * TEXT
     */
    public static function text($paragraphs = null, $lengths = null, $tags = array('p')) {
        $url = 'http://loripsum.net/api';

        if ($paragraphs && is_numeric($paragraphs))
            $url .= '/' . (int)$paragraphs;

        if ($lengths && in_array($lengths, array('short', 'medium', 'long', 'verylong')))
            $url .= '/' . $lengths;

        $addedTags = false;

        if (!empty($tags) && is_array($tags)) {
            if (in_array('p', $tags))
                $addedTags = true;

            foreach ($tags as $t) {
                if ($p = self::_textTagParam($t)) {
                    $url .= '/' . $p;
                    $addedTags = true;
                }
            }
        }

        if (!$addedTags)
            $url .= '/plaintext';

        $text = file_get_contents($url);

        if ($addedTags) {
            if (in_array('strong', $tags))
                $text = self::_textTagReplace($text, 'b', 'strong');

            if (in_array('em', $tags))
                $text = self::_textTagReplace($text, 'i', 'em');

            if (in_array('ul', $tags) || in_array('li', $tags))
                $tags[] = 'li';

            if (in_array('dl', $tags)) {
                $tags[] = 'dt';
                $tags[] = 'dd';
            }

            $text = self::_textStripTags($text, $tags);

            $text = str_replace("<a href='http://loripsum.net/' target='_blank'>", '<a href="#">', $text);
        }

        return $text;
    }

    private static function _textTagParam($tag) {
        switch ($tag) {
            case 'a':
                return 'link';
            case 'ul':
                return 'ul';
            case 'ol':
                return 'ol';
            case 'dl':
                return 'dl';
            case 'blockquote':
                return 'bq';
            case 'code':
                return 'code';
        }

        if (in_array($tag, array('b', 'i', 'mark', 'strong', 'em')))
            return 'decorate';

        if (preg_match('/^h\d$/', $tag))
            return 'headers';

        return null;
    }

    private static function _textTagReplace($text, $search, $replace) {
        return preg_replace('#<(/?)' . $search . '>#i', '<$1' . $replace . '>', $text);
    }

    private static function _textStripTags($text, $tags = array()) {
        if (count($tags))
            $allowed = '<' . implode('><', $tags) . '>';
        else
            $allowed = '';

        return strip_tags($text, $allowed);
    }

    public static function corporate($words) {
        $text = 'Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.

Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.

Completely synergize resource sucking relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service.

Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate extensible testing procedures for reliable supply chains. Dramatically engage top-line web services vis-a-vis cutting-edge deliverables.

Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.

Phosfluorescently engage worldwide methodologies with web-enabled technology. Interactively coordinate proactive e-commerce via process-centric "outside the box" thinking. Completely pursue scalable customer service through sustainable potentialities.

Collaboratively administrate turnkey channels whereas virtual e-tailers. Objectively seize scalable metrics whereas proactive e-services. Seamlessly empower fully researched growth strategies and interoperable internal or "organic" sources.

Credibly innovate granular internal or "organic" sources whereas high standards in web-readiness. Energistically scale future-proof core competencies vis-a-vis impactful experiences. Dramatically synthesize integrated schemas with optimal networks.

Interactively procrastinate high-payoff content without backward-compatible data. Quickly cultivate optimal processes and tactical architectures. Completely iterate covalent strategic theme areas via accurate e-markets.

Globally incubate standards compliant channels before scalable benefits. Quickly disseminate superior deliverables whereas web-enabled applications. Quickly drive clicks-and-mortar catalysts for change before vertical architectures.

Credibly reintermediate backend ideas for cross-platform models. Continually reintermediate integrated processes through technically sound intellectual capital. Holistically foster superior methodologies without market-driven best practices.

Distinctively exploit optimal alignments for intuitive bandwidth. Quickly coordinate e-business applications through revolutionary catalysts for change. Seamlessly underwhelm optimal testing procedures whereas bricks-and-clicks processes.

Synergistically evolve 2.0 technologies rather than just in time initiatives. Quickly deploy strategic networks with compelling e-business. Credibly pontificate highly efficient manufactured products and enabled data.

Dynamically target high-payoff intellectual capital for customized technologies. Objectively integrate emerging core competencies before process-centric communities. Dramatically evisculate holistic innovation rather than client-centric data.

Progressively maintain extensive infomediaries via extensible niches. Dramatically disseminate standardized metrics after resource-leveling processes. Objectively pursue diverse catalysts for change for interoperable meta-services.

Proactively fabricate one-to-one materials via effective e-business. Completely synergize scalable e-commerce rather than high standards in e-services. Assertively iterate resource maximizing products after leading-edge intellectual capital.

Distinctively re-engineer revolutionary meta-services and premium architectures. Intrinsically incubate intuitive opportunities and real-time potentialities. Appropriately communicate one-to-one technology after plug-and-play networks.

Quickly aggregate B2B users and worldwide potentialities. Progressively plagiarize resource-leveling e-commerce through resource-leveling core competencies. Dramatically mesh low-risk high-yield alignments before transparent e-tailers.

Appropriately empower dynamic leadership skills after business portals. Globally myocardinate interactive supply chains with distinctive quality vectors. Globally revolutionize global sources through interoperable services.

Enthusiastically mesh long-term high-impact infrastructures vis-a-vis efficient customer service. Professionally fashion wireless leadership rather than prospective experiences. Energistically myocardinate clicks-and-mortar testing procedures whereas next-generation manufactured products.';

        $text = str_replace(array('.', ','), ' ', $text);
        $text = preg_replace('/\s+/', ' ', $text);
        $text = explode(' ', $text);

        $rand = mt_rand(0, count($text) - $words - 1);

        return implode(' ', array_slice($text, $rand, $words));
    }
    
    
    /*
     * IMAGES
     */
    public static function image($width, $height, $type = 'generic') {
        $types = array(
            'generic' => 'placehold.it',
            'abstract' => 'lorempixel',
            'animals' => 'lorempixel',
            'business' => 'lorempixel',
            'cats' => 'lorempixel',
            'city' => 'lorempixel',
            'food' => 'lorempixel',
            'nightlife' => 'lorempixel',
            'fashion' => 'lorempixel',
            'people' => 'lorempixel',
            'nature' => 'lorempixel',
            'sports' => 'lorempixel',
            'technics' => 'lorempixel',
            'transport' => 'lorempixel',
        );

        if (!isset($types[$type]))
            $type = 'generic';

        switch ($types[$type]) {
            case 'placehold.it':
                return self::placeholdIt($width, $height);
            case 'lorempixel':
                return self::loremPixel($width, $height, $type);
        }

        return null;
    }

    public static function placeholdIt($width, $height, $backgroundColor = null, $textColor = null, $text = null) {
        $format = 'http://placehold.it/%dx%d';

        if ($backgroundColor || $textColor) {
            $backgroundColor = $backgroundColor ? self::_colorTrim($backgroundColor) : 'cccccc';
            $textColor = $textColor ? self::_colorTrim($textColor) : '969696';
            $format .= '/%s/%s';
        }

        $url = sprintf(
            $format,
            $width,
            $height,
            $backgroundColor,
            $textColor
        );

        if ($text)
            $url .= '/&text=' . urlencode($text);

        return array($url, $width, $height);
    }

    public static function loremPixel($width, $height, $type = null, $greyscale = false, $index = null, $text = null) {
        $url = 'http://lorempixel.com';
        if ($greyscale)
            $url .= '/g';

        $format = '/%d/%d';

        if (($index || $text) && !$type)
            $type = 'abstract';

        if ($type)
            $format .= '/%s';

        $url .= sprintf(
            $format,
            $width,
            $height,
            $type
        );

        if ($index)
            $url .= '/' . $index;

        if ($text)
            $url .= '/' . $text;

        return $url;
    }

    private static function _colorTrim($color) {
        return preg_replace('/[^0-9a-f]/i', '', $color);
    }
}