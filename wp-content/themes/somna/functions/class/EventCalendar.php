<?php
class EventCalendar
{
    public static $dayNames = [];
    public static $monthNames = [];

    public static function init()
    {
        self::$dayNames = [
            'Mon' => __('Monday', THEME_TEXT),
            'Tue' => __('Tuesday', THEME_TEXT),
            'Wed' => __('Wednesday', THEME_TEXT),
            'Thu' => __('Thursday', THEME_TEXT),
            'Fri' => __('Friday', THEME_TEXT),
            'Sat' => __('Saturday', THEME_TEXT),
            'Sun' => __('Sunday', THEME_TEXT),
        ];

        self::$monthNames = [
            'Jan' => __('January', THEME_TEXT),
            'Feb' => __('February', THEME_TEXT),
            'Mar' => __('March', THEME_TEXT),
            'Apr' => __('April', THEME_TEXT),
            'May' => __('May', THEME_TEXT),
            'Jun' => __('June', THEME_TEXT),
            'Jul' => __('July', THEME_TEXT),
            'Aug' => __('August', THEME_TEXT),
            'Sep' => __('September', THEME_TEXT),
            'Oct' => __('October', THEME_TEXT),
            'Nov' => __('November', THEME_TEXT),
            'Dec' => __('December', THEME_TEXT),

        ];

        add_action('init', function () {
            $args = array(
                'public' => true,
                'show_ui' => true,
                'label' => __('Events', THEME_TEXT),
                'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
                'menu_position' => 4,
                'taxonomies' => array('category', 'post_tag')
            );
            register_post_type('events', $args);
        });

        add_shortcode('event_calendar', array(__CLASS__, 'events'));
    }
    public static function getEventPosts($coming=false, $post_per_page=-1){

        $meta_query = [];

        if($coming){
            $meta_query = array(
                array(
                    'key' => 'event-end',
                    'value' => date('Ymd'),
                    'compare' => '>='
                )
            );
        }

        $events = get_posts(array(
            'posts_per_page' => $post_per_page,
            'post_type'		=> 'events',
            'suppress_filters' => 0,
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_key' => 'event-start',
            'meta_query' => $meta_query
        ));
        return $events;
    }
    public static function getComingEvents(){
        $html = '<div class="sidebar">';
        $html .= '<div class="widget">';
        $html .= '<h3 class="widget__title">'.__('Coming events', THEME_TEXT).'</h3>';
        $html .= '<ul>';
        $events = self::getEventPosts(true);

        foreach ($events  as $event ) {
            $eventId = $event->ID;
            $start_date = self::formatDate(get_field('event-start', $event->ID));
            $end_date = self::formatDate(get_field('event-end', $event->ID));
            $html .= '<li><span>'.$start_date.' - '.$end_date.'</span><br><a href="'.get_the_permalink($eventId).'">'.$event->post_title.'</a></li>';
        }
        $html .= '</ul>';
        $html .= '<a class="hidden-max-phone" href="'.get_permalink(get_field('event_page','options')).'">'.__('All Events', THEME_TEXT).'</a>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
    public static function formatDate($date, $format='Y-m-d'){
        $date = new DateTime($date);
        $date = $date->format($format);
        return $date;
    }
    public static function events(){
        echo '<div id="event-calendar"></div>';
    }
    public static function getMonth($year, $month, $day)
    {
        $date = date("Y-m-d", mktime(0, 0, 0, $month, $day, $year));
        $calendar = new SimpleCalendar($date);
        $calendar->setStartOfWeek('Monday');

        $html = '';
        $years = [];
        $events = self::getEventPosts();
        foreach ($events  as $event ) {
            $eventId = $event->ID;
            $link = '<a href="'.get_the_permalink($eventId).'">'.$event->post_title.'</a>';
            $event_start_date = get_field('event-start', $eventId);
            $event_end_date = get_field('event-end', $eventId);
            $calendar->addDailyHtml( $link, $event_start_date, $event_end_date );
            $y = self::formatDate($event_start_date,'Y');
            if (!in_array($y, $years))
            {
                $years[] = $y;
            }
        }
        /*for($i=0; $i<2; $i++){
            $y = intval(end($years)) + 1;
            $years[] = $y;
        }*/

        $monthName = date("M", mktime(0, 0, 0, $month, $day, $year));
        $yearName = date("Y", mktime(0, 0, 0, $month, $day, $year));


        $translatedMonthName = self::$monthNames[$monthName];
        $html .= '<h2 class="title hidden-max-phone">'.$translatedMonthName.' '.$yearName.'</h2>';
        $html .= '<div class="buttons hidden-max-phone">';
        $html .= '<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i><i class="fa fa-angle-left" aria-hidden="true"></i></a><div class="selects"><select class="months">'.self::getSelect(self::$monthNames, $translatedMonthName, true).'</select></div><div class="selects" style="width:70px;"><select class="years">'.self::getSelect($years, $yearName).'</select></div>';
        if ($monthName=='Dec' && !in_array($yearName+1, $years)){
            //$html .= '<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i><i class="fa fa-angle-right" aria-hidden="true"></i></a>';
        }else{
            $html .= '<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i><i class="fa fa-angle-right" aria-hidden="true"></i></a>';
        }
        $html .= '</div>';
        $html .= '<div style="clear: both"></div>';
        $html .= '<div class="hidden-max-phone">';
        $html .= $calendar->show(false);
        $html .= '</div>';
        $html .= '<div class="visible-max-phone">';
        $html .= self::getComingEvents();
        $html .= '</div>';
        return $html;
    }
    private static function getSelect($months, $monthName, $useCC=false) {
        $options = '';
        $cc = 1;
        foreach ($months as $month){
            $selected = '';
            if($monthName == $month){
                $selected = 'selected';
            }
            $val = $month;
            if($useCC){
                $val = $cc;
            }
            $options .= '<option '.$selected.' value="'.$val.'">'.$month.'</option>';
            $cc++;
        }
        return $options;
    }
    public static function getMonthName($year, $month, $day)
    {
        $monthName = date("F", mktime(0, 0, 0, $month, $day, $year));
        return $monthName;
    }
}
EventCalendar::init();