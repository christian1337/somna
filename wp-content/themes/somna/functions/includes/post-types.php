<?php

$args = array(
    'public' => true,
    'show_ui' => true,
    'label'  => 'Case',
    'supports' => array( 'title', 'thumbnail', 'editor', 'excerpt' ),
    'menu_position' => 5,
    'has_archive'   => true,
);
register_post_type( 'case', $args );

register_taxonomy(
    'case_cat',
    'case',
    array(
        'label' => __( 'Categories' ),
        'rewrite' => array( 'slug' => 'case_cat' ),
        'hierarchical' => true,
    )
);

$args = array(
    'public' => true,
    'show_ui' => true,
    'label'  => 'Anv.område',
    'supports' => array( 'title', 'thumbnail', 'editor', 'excerpt' ),
    'menu_position' => 6,
    'has_archive'   => true,
);
register_post_type( 'area', $args );

register_taxonomy(
    'area_cat',
    'area',
    array(
        'label' => __( 'Categories' ),
        'rewrite' => array( 'slug' => 'area_cat' ),
        'hierarchical' => true,
    )
);