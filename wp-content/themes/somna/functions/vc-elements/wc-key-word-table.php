<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class WcKeyWordsTable extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_keywordstable_mapping' ) );
        add_shortcode( 'wc_keywordstable', array( $this, 'wc_keywordstable_html' ) );
    }

    // Element Mapping
    public function wc_keywordstable_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co key words table', 'text-domain'),
                'base' => 'wc_keywordstable',
                'description' => __('', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    VcElementAsset::textfield('table_header', 'Header'),
                    VcElementAsset::dropdown('table_text_align', ['center','left','right'], 'Header text align', 'Settings'),
                    VcElementAsset::colorpicker('table_txt_color', 'Text color', 'Settings'),
                    VcElementAsset::colorpicker('table_bg', 'Background color', 'Settings'),
                    VcElementAsset::dropdown('table_columns', [3,2,1], 'Columns', 'Settings'),
                    array(
                        'type' => 'param_group',
                        'param_name' => 'words',
                        'group' => 'General',
                        'heading' => __('Add word', 'text-domain'),
                        'params' => array(
                            VcElementAsset::textfield('word_value', 'Text'),
                        )
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_keywordstable_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'table_header' => '',
            'words' => null,
            'table_bg' => '#eef0f2',
            'table_txt_color' => '#000000',
            'table_columns' => 3,
            'table_text_align' => 'center'
        ),
            $atts ));

        $words = vc_param_group_parse_atts( $atts['words'] );
        $words_arrays = partition($words, $table_columns);
        $class = 'columns_'.$table_columns;
        $cc=0;
        ob_start();
        ?>
        <div class="wc-word-table <?php echo $class;?>" style="background-color: <?php echo $table_bg;?>; color: <?php echo $table_txt_color;?>;">
                <?php
                    echo '<h2 style="padding-left:15px; padding-right:15px; text-align: '.$table_text_align.'">'.$table_header.'</h2>';
                    foreach ($words_arrays as $word_array){
                        $cc ++;
                        ?>
                        <div class="col col_<?php echo $cc; ?>">
                        <?php
                        foreach ($word_array as $word) {
                            ?>
                            <?php echo $word['word_value'] ?><br>
                            <?php
                        }
                        ?>
                        </div>
                        <?php
                    }
                ?>
            <div style="clear:both;"></div>
        </div>
        <?php
        return ob_get_clean();
    }

} // End Element Class

function partition( $list, $p ) {
    $listlen = count( $list );
    $partlen = floor( $listlen / $p );
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice( $list, $mark, $incr );
        $mark += $incr;
    }
    return $partition;
}

// Element Class Init
new WcKeyWordsTable();