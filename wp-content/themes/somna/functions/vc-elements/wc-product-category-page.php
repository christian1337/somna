<?php
/*
Element Description: Get all products fron custom taxanomy brand.
*/

// Element Class
class WcBrandProductWidget extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_brand_posts_widget_mapping' ) );
        add_shortcode( 'wc_brand_posts_widget', array( $this, 'wc_brand_posts_widget_html' ) );
    }

    // Element Mapping
    public function wc_brand_posts_widget_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Product list', 'text-domain'),
                'base' => 'wc_brand_posts_widget',
                'description' => __('List products on main product category page', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
            )
        );
    }

    // Element HTML
    public function wc_brand_posts_widget_html( $atts, $content = null ) {
        ob_start();
        ?>
        <?php

        //$_term_slug =  get_queried_object()->slug;
        $_term_slug = MainProductCat::getSlug();

        $query_args = array(
            'numberposts'	=> -1,
            'post_type'		=> 'product',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'terms' => $_term_slug,
                    'field' => 'slug',
                    'include_children' => true,
                    'operator' => 'IN'
                )
            ),
        );

        $query = new WP_Query($query_args);
        ?>
        <?php if($query->have_posts()) : ?>
            <ul class="products">
            <?php while($query->have_posts()) : $query->the_post();
                wc_get_template_part( 'content', 'product' );
            endwhile; ?>
             </ul>
        <?php else : ?>

            <p><?php _e('No products found for this category.', THEME_TEXT);?></p>

        <?php endif; ?>

        <?php wp_reset_postdata();
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new WcBrandProductWidget();