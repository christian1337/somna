<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class wcFeaturedContent extends WPBakeryShortCode
{

    // Element Init
    function __construct()
    {
        add_action('init', array($this, 'wc_featured_content_mapping'));
        add_shortcode('wc_featured_content', array($this, 'wc_featured_content_html'));
    }

    // Element Mapping
    public function wc_featured_content_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Featured content', 'text-domain'),
                'base' => 'wc_featured_content',
                'description' => __('Images and links grid', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri() . '/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __('Image', 'text-domain'),
                        'param_name' => 'image_1',
                        'value' => '',
                        'description' => __('Select image from media library.', 'text-domain'),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Large image',
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header_1',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Large image',
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => __('Link to', 'text-domain'),
                        'param_name' => 'url_1',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Large image',
                    ),
                    array(
                        'type' => 'attach_image',
                        'heading' => __('Image', 'text-domain'),
                        'param_name' => 'image_2',
                        'value' => '',
                        'description' => __('Select image from media library.', 'text-domain'),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Top right small image',
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header_2',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Top right small image',
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => __('Link to', 'text-domain'),
                        'param_name' => 'url_2',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Top right small image',
                    ),
                    array(
                        'type' => 'attach_image',
                        'heading' => __('Image', 'text-domain'),
                        'param_name' => 'image_3',
                        'value' => '',
                        'description' => __('Select image from media library.', 'text-domain'),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Bottom right small image',
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header_3',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Bottom right small image',
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => __('Link to', 'text-domain'),
                        'param_name' => 'url_3',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Bottom right small image',
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_featured_content_html($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'image_1' => '',
            'url_1' => '',
            'header_1' => '',
            'image_2' => '',
            'url_2' => '',
            'header_2' => '',
            'image_3' => '',
            'url_3' => '',
            'header_3' => '',
        ),
            $atts));

        $href_1 = vc_build_link($url_1);
        $href_2 = vc_build_link($url_2);
        $href_3 = vc_build_link($url_3);
        ob_start();
        ?>
        <section class="section-features">
            <div class="features">
                <div class="feature feature--large">
                    <div class="feature__body">
                        <a href="<?php echo $href_1['url'] ?>" target="<?php echo $href_1['target'] ?>">
                            <div class="feature__image fullsize-image">
                                <img src="<?php echo wp_get_attachment_url($image_1, 'full') ?>">
                            </div>
                            <div class="feature__content left">
                                <span class="btn btn-feature">
                                    <div class="header"><?php echo $header_1; ?></div>
                                    <?php echo $href_1['title'] ?>
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                </span>
                            </div>
                        </a>
                    </div>
                </div>
               <div class="feature feature--small">
                    <div class="feature__body">
                        <a href="<?php echo $href_2['url'] ?>" target="<?php echo $href_2['target'] ?>">
                            <div class="feature__image fullsize-image">
                                <img src="<?php echo wp_get_attachment_url($image_2, 'full') ?>">
                            </div>
                            <div class="feature__content">
                                <span class="btn btn-feature">
                                    <div class="header"><?php echo $header_2; ?></div>
                                    <?php echo $href_2['title'] ?>
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                </span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="feature feature--small feature--small-alt">
                    <div class="feature__body">
                        <a href="<?php echo $href_3['url'] ?>" target="<?php echo $href_3['target'] ?>">
                            <div class="feature__image fullsize-image">
                                <img src="<?php echo wp_get_attachment_url($image_3, 'full') ?>">
                            </div>
                            <div class="feature__content">
                                        <span class="btn btn-feature">
                                            <div class="header"><?php echo $header_3; ?></div>
                                            <?php echo $href_3['title'] ?>
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new wcFeaturedContent();