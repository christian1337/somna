<?php
class WcBootstrapFullWidth extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_bootstrap_full_width_mapping' ) );
        add_shortcode( 'wc_bootstrap_full_width', array( $this, 'wc_bootstrap_full_width_html' ) );
    }

    // Element Mapping
    public function wc_bootstrap_full_width_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('W&Co Full width bar', 'text-domain'),
                'base' => 'wc_bootstrap_full_width',
                'description' => __('Add container-full-width class to row', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'textarea_html',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'value' => __( '' ),
                        'description' => __( 'Text', 'text-domain' ),
                        'admin_label' => false,
                        'group' => 'General',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => __( 'Background color', 'text-domain' ),
                        'param_name' => 'bgcolor',
                        'value' => __( '' ),
                        'admin_label' => false,
                        'group' => 'Settings',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => __( 'Text color', 'text-domain' ),
                        'param_name' => 'txtcolor',
                        'value' => __( '#000000' ),
                        'admin_label' => false,
                        'group' => 'Settings',
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_bootstrap_full_width_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'bgcolor' => '',
            'txtcolor' => ''
        ),
            $atts ));

        ob_start();
        ?>
        <div class="bar" style="background-color: <?php echo $bgcolor; ?>; color: <?php echo $txtcolor; ?>;">
            <div class="container">
                <?php echo $content;?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
} // End Element Class

// Element Class Init
new WcBootstrapFullWidth();