<?php
class WcPagePushRegular extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_page_push_regular__mapping' ) );
        add_shortcode( 'wc_page_push_regular', array( $this, 'wc_page_push_regular_html' ) );
    }

    // Element Mapping
    public function wc_page_push_regular__mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Page push regular', 'text-domain'),
                'base' => 'wc_page_push_regular',
                'description' => __('', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    VcElementAsset::dropdown('txt_align', ['left', 'center', 'right'], 'Text align'),
                    VcElementAsset::dropdown('font_size', ['medium', 'large'], 'Font size'),
                    VcElementAsset::textarea_html(),
                    VcElementAsset::colorpicker('bg_color', 'Background Color'),
                    VcElementAsset::dropdown('icon', ['Select background icon', 'qoute'], 'Icon'),
                    VcElementAsset::vc_link(),
                )
            )
        );
    }

    // Element HTML
    public function wc_page_push_regular_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'url'   => '',
            'icon'  => '',
            'bg_color' => '#f6f7f8',
            'txt_align' => 'left',
            'font_size' => 'medium'
        ),
            $atts ));

        $href = vc_build_link($url);
        ob_start();
        ?>
        <div class="push-regular <?php echo $icon; ?> <?php echo 'f-'.$font_size?>" style="background-color: <?php echo $bg_color; ?>; text-align: <?php echo $txt_align; ?>">
            <article class="content">
                <?php echo $content;
                if ($href['url']){
                    echo '<a href="'.$href['url'].'" target="'.$href['target'].'">'.$href['title'].'</a>';
                }
                ?>
            </article>
        </div>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new WcPagePushRegular();