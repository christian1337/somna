<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class wcArticle extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_article_mapping' ) );
        add_shortcode( 'wc_article', array( $this, 'wc_article_html' ) );
    }

    // Element Mapping
    public function wc_article_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Article', 'text-domain'),
                'base' => 'wc_article',
                'description' => __('Article with preamble', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'General',
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => __( 'Subheader', 'text-domain' ),
                        'param_name' => 'subheader',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 1,
                        'group' => 'General',
                    ),
                    VcElementAsset::checkbox('use_blue_line', '', 'Use blue line below header'),
                    array(
                        'type' => 'textarea_html',
                        'heading' => __( 'Content', 'text-domain' ),
                        'param_name' => 'content',
                        'value' => __( '' ),
                        'description' => __( 'Text', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 2,
                        'group' => 'General',
                    ),
                    VcElementAsset::vc_link(),
                )
            )
        );
    }

    // Element HTML
    public function wc_article_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'header' => '',
            'subheader'   => '',
            'url'   => '',
        ),
            $atts ));

        $href = vc_build_link($url);
        $class = '';
        if($atts['use_blue_line']){
            $class = 'line';
        }

        ob_start();
        ?>
        <article class="wc-article">
        <?php if ($subheader){
            echo '<h1 class="small">'.$header.'</h1><h2 class="'.$class.'">'.$subheader.'</h2>';
        } else {
            echo '<h1 class="'.$class.'">'.$header.'</h1>';
        }
            echo '<p>'.$content.'</p>';
            if ($href['url']){
                echo '<div class="c-link"><a href="'.$href['url'].'" target="'.$href['target'].'">'.$href['title'].'</a></div>';
            }
        ?>
        </article>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new wcArticle();