<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class wcContact extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_contact_mapping' ) );
        add_shortcode( 'wc_contact', array( $this, 'wc_contact_html' ) );
    }

    // Element Mapping
    public function wc_contact_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Contact', 'text-domain'),
                'base' => 'wc_contact',
                'description' => __('Contact person with image', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __( 'Image', 'text-domain' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => __( 'Select image from media library.', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact left',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact left',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'phone',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact left',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'email',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact left',
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => __( 'Contact information', 'text-domain' ),
                        'param_name' => 'content_l',
                        'value' => __( '' ),
                        'description' => __( 'Text', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact left',
                    ),
                    array(
                        'type' => 'attach_image',
                        'heading' => __( 'Image', 'text-domain' ),
                        'param_name' => 'image_r',
                        'value' => '',
                        'description' => __( 'Select image from media library.', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact right',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header_r',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact right',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'phone_r',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact left',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'email_r',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact left',
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => __( 'Contact information', 'text-domain' ),
                        'param_name' => 'content_r',
                        'value' => __( '' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Contact right',
                    ),
                )
            )
        );
    }

    // Element HTML
    public function wc_contact_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'image'   => '',
            'image_r' => '',
            'content_l' => '',
            'content_r' => '',
            'header' => '',
            'header_r' => ''
        ),
            $atts ));
        ob_start();
        ?>
        <div class="contact-container">
            <div class="contact left">
                <div class="contact-image"><img src="<?php echo wp_get_attachment_url($image, 'full') ?>"/> </div>
                <div class="intro__content"><h3><?php echo $header ?></h3><?php echo $content_l ?></div>
            </div>
            <div class="contact right">
                <div class="contact-image"><img src="<?php echo wp_get_attachment_url($image_r, 'full') ?>"/> </div>
                <div class="intro__content"><h3><?php echo $header_r ?></h3><?php echo $content_r ?></div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new wcContact();