<?php
class WcPageIntro extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_page_intro_mapping' ) );
        add_shortcode( 'wc_page_intro', array( $this, 'wc_page_intro_html' ) );
    }

    // Element Mapping
    public function wc_page_intro_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Page intro', 'text-domain'),
                'base' => 'wc_page_intro',
                'description' => __('Image push with text and icon', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    VcElementAsset::attach_image('image_left', 'Column left'),
                    VcElementAsset::textarea_html('Content', 'Column left'),
                    VcElementAsset::attach_image('image_right_1', 'Column right 1'),
                    VcElementAsset::dropdown('icon_right_1', ['day', 'night', 'sensitive', 'private', 'pro', 'none'], 'Icon', 'Column right 1'),
                    VcElementAsset::vc_link('url_right_1', 'Link to', 'Column right 1'),
                    VcElementAsset::attach_image('image_right_2', 'Column right 2'),
                    VcElementAsset::dropdown('icon_right_2', ['day', 'night', 'sensitive', 'private', 'pro', 'none'], 'Icon', 'Column right 2'),
                    VcElementAsset::vc_link('url_right_2', 'Link to', 'Column right 2'),
                )
            )
        );
    }

    // Element HTML
    public function wc_page_intro_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'image_left' => '',
            'image_right_1' => '',
            'url_right_1'   => '',
            'icon_right_1'  => 'day',
            'image_right_2' => '',
            'url_right_2'   => '',
            'icon_right_2'  => 'day'
        ),
            $atts ));

        $href_right_1 = vc_build_link( $url_right_1);
        $href_right_2 = vc_build_link( $url_right_2);

        ob_start();
        ?>
        <div class="intro">
            <div class="col-left">
                <div class="push-item">
                    <div class="hover-image" style="background-image: url('<?php echo wp_get_attachment_url($image_left, 'full')  ?>')"> </div>
                    <div class="content">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
            <div class="col-right">
                <a href="<?php echo $href_right_1['url'] ?>" target="<?php echo $href_right_1['target'] ?>">
                    <div class="push-item">
                        <div class="hover-image" style="background-image: url('<?php echo wp_get_attachment_url($image_right_1, 'full')  ?>')"> </div>
                        <div class="content <?php echo Icons::icon_content_class($icon_right_1); ?>">
                            <?php if(Icons::icon($icon_right_1)) { ?>
                                <div class="icon"><?php echo Icons::icon($icon_right_1); ?></div>
                            <?php } ?>
                            <div class="title"><?php echo $href_right_1['title'] ?></div>
                        </div>
                    </div>
                </a>
                <a href="<?php echo $href_right_2['url'] ?>" target="<?php echo $href_right_2['target'] ?>">
                    <div class="push-item last">
                        <div class="hover-image" style="background-image: url('<?php echo wp_get_attachment_url($image_right_2, 'full')  ?>')"> </div>
                        <div class="content <?php echo Icons::icon_content_class($icon_right_2); ?>">
                            <?php if(Icons::icon($icon_right_2)) { ?>
                                <div class="icon"><?php echo Icons::icon($icon_right_2); ?></div>
                            <?php } ?>
                            <div class="title"><?php echo $href_right_2['title'] ?></div>
                        </div>
                    </div>
                </a>
            </div>
            <div style="clear: both"></div>
        </div>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new WcPageIntro();