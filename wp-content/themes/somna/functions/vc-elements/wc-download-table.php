<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class WcDownloadTable extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_downloads_mapping' ) );
        add_shortcode( 'wc_downloads', array( $this, 'wc_downloads_html' ) );
    }

    // Element Mapping
    public function wc_downloads_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co downloads', 'text-domain'),
                'base' => 'wc_downloads',
                'description' => __('', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    VcElementAsset::textfield('table_header', 'Header'),
                    VcElementAsset::colorpicker('table_txt_color', 'Text color'),
                    VcElementAsset::colorpicker('table_bg', 'Background color'),
                    VcElementAsset::textfield('left_header', 'Header', 'Left Column'),
                    VcElementAsset::textfield('right_header', 'Header', 'Right Column'),
                    array(
                        'type' => 'param_group',
                        'param_name' => 'links_left',
                        'group' => 'Left Column',
                        'heading' => __('Add link', 'text-domain'),
                        'params' => array(
                            VcElementAsset::textfield('left_text_value', 'Text'),
                            VcElementAsset::textfield('left_link_value', 'Url'),
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'param_name' => 'links_right',
                        'group' => 'Right Column',
                        'heading' => __('Add link', 'text-domain'),
                        'params' => array(
                            VcElementAsset::textfield('right_text_value', 'Text'),
                            VcElementAsset::textfield('right_link_value', 'Url'),
                        )
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_downloads_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'table_header' => '',
            'links_left' => null,
            'links_right' => null,
            'left_header' => '',
            'right_header' => '',
            'table_bg' => '#eef0f2',
            'table_txt_color' => '#000000'
        ),
            $atts ));

        $links_left = vc_param_group_parse_atts( $atts['links_left'] );
        $links_right = vc_param_group_parse_atts( $atts['links_right'] );

        ob_start();
        ?>
        <div class="wc-downloads" style="background-color: <?php echo $table_bg;?>; color: <?php echo $table_txt_color;?>;">
            <h2><?php echo $table_header; ?></h2>
            <div class="col">
                <h3><?php echo $left_header; ?></h3>
                <ul>
                    <?php foreach ($links_left as $link_left){
                    echo self::getLi($link_left['left_link_value'], $link_left['left_text_value']);
                    } ?>
                </ul>
            </div>
            <div class="col">
                <h3><?php echo $right_header; ?></h3>
                <ul>
                    <?php foreach ($links_right as $link_right){
                        echo self::getLi($link_right['right_link_value'], $link_right['right_text_value']);
                    } ?>
                </ul>
            </div>
            <div style="clear:both;"></div>
        </div>
        <?php
        return ob_get_clean();
    }

    private static function getLi($url, $name){
        $class = 'dot';
        if($url){
            $class = 'url';
            return sprintf('<li class="%s"><a href="%s">%s</a></li>', $class, $url, $name);
        }else{
            return sprintf('<li class="%s">%s</li>', $class, $name);
        }

    }

} // End Element Class

// Element Class Init
new WcDownloadTable();