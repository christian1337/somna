<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class wcCallOut extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_call_out_mapping' ) );
        add_shortcode( 'wc_call_out', array( $this, 'wc_call_out_html' ) );
    }

    // Element Mapping
    public function wc_call_out_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Call out', 'text-domain'),
                'base' => 'wc_call_out',
                'description' => __('Call out with button', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'textarea_html',
                        'heading' => __( 'Content', 'text-domain' ),
                        'holder' => 'p',
                        'class' => 'title-class',
                        'param_name' => 'content',
                        'value' => __( '' ),
                        'description' => __( 'Text', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 2,
                        'group' => 'General',
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => __( 'Button', 'text-domain' ),
                        'param_name' => 'url',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'General',
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_call_out_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'url' => '',
        ),
            $atts ));

        $href = vc_build_link( $url);
        ob_start();
        ?>
        <div class="widget-callout">
            <div class="widget__body">
                <div class="callout callout--alt">
                    <div class="callout__body">
                        <?php echo $content;?>
                    </div>
                    <div class="callout__actions">
                        <a href="<?php echo $href['url'] ?>" target="<?php echo $href['target'] ?>" class="btn btn-default"><?php echo $href['title'] ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new wcCallOut();