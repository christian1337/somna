<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class wcLogoGrid extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_logo_grid_mapping' ) );
        add_shortcode( 'wc_logo_grid', array( $this, 'wc_logo_grid_html' ) );
    }

    // Element Mapping
    public function wc_logo_grid_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Logo grid', 'text-domain'),
                'base' => 'wc_logo_grid',
                'description' => __('Grid of logos on white background', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'General',
                    ),
                    array(
                        'type' => 'attach_images',
                        'heading' => __( 'Logos', 'text-domain' ),
                        'param_name' => 'images',
                        'value' => '',
                        'description' => __( 'Select images from media library.', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'General',
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_logo_grid_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'images' => '',
            'header' => ''
        ),
            $atts ));

        //var_dump($images);
        $images = explode(",", $images);
        ob_start();
        ?>
            <div class="widget widget--logos">
                <header class="widget__head">
                    <h5 class="widget__title"><?php echo $header;?></h5>
                <div class="widget__body">
                    <ul class="list-logos">
                        <?php foreach ($images as $i) { ?>
                        <li>
                            <span>
                                <img src="<?php echo wp_get_attachment_url($i, 'full') ?>" alt="">
                            </span>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

        <?php
        return ob_get_clean();
    }
}

// Element Class Init
new wcLogoGrid();