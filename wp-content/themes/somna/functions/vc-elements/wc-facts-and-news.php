<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class WcFactsAndNews extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_factsandnews_mapping' ) );
        add_shortcode( 'wc_factsandnews', array( $this, 'wc_factsandnews_html' ) );
    }

    // Element Mapping
    public function wc_factsandnews_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __('W&Co facts & news', 'text-domain'),
                'base' => 'wc_factsandnews',
                'description' => __('Multiple facts and latsest news', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    VcElementAsset::textfield('facts_header', 'Header', 'Facts'),
                    VcElementAsset::colorpicker('facts_txt_color', 'Text color', 'Facts'),
                    VcElementAsset::colorpicker('facts_bg', 'Background color', 'Facts'),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'facts',
                        'group' => 'Facts',
                        'heading' => __('Add fact', 'text-domain'),
                        'params' => array(
                            VcElementAsset::textfield('counter_value', 'Counter value (integer)'),
                            VcElementAsset::textfield('counter_format', 'Counter format (e.g. %)'),
                            VcElementAsset::textarea('counter_desc', 'Description')
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'news_links',
                        'group' => 'News',
                        'heading' => __('Add Link', 'text-domain'),
                        'description' => __('Add links to the latest news section', 'text-domain'),
                        'params' => array(
                            VcElementAsset::vc_link('news_link')
                        )
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_factsandnews_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'facts_header' => '',
            'facts' => null,
            'facts_bg' => '',
            'facts_txt_color' => '#FFFFFF'
        ),
            $atts ));
        $links= vc_param_group_parse_atts( $atts['news_links'] );
        $facts = vc_param_group_parse_atts( $atts['facts'] );
        $query_args = array(
            'post_type' => 'post',
            'posts_per_page' => 3
        );
        $query = new WP_Query($query_args);

        ob_start();
        ?>
        <div class="wc-facts-and-news">
            <div class="col-1">
                <div class="facts" style="background-color: <?php echo $facts_bg;?>; color: <?php echo $facts_txt_color;?>;">
                <?php
                    echo '<span class="facts-header">'.$facts_header.'</span>';
                    foreach ($facts as $fact){
                        ?>
                        <div class="fact">
                        <div class="fact-value"><span><?php echo $fact['counter_value']?></span><span class="fact-format">&nbsp;<?php echo $fact['counter_format']?></span></div>
                        <div class="fact-desc"><?php echo $fact['counter_desc']?></div>
                        </div>
                        <?php
                    }
                ?>
                </div>
            </div>
            <div class="col-2">
                <div class="latest-news">
                    <div class="news-links">
                        <h2 style="margin-top: -10px;"><?php echo get_the_title( get_option('page_for_posts', true) );?></h2>
                        <?php while($query->have_posts()) : $query->the_post();
                            $image_url = get_the_post_thumbnail_url();
                            $class = 'full-width';
                        ?>
                        <article>
                            <?php if ( $image_url ) { $class=''?> <div class="news-image" style="background-image: url(<?php echo $image_url?>);"></div><?php } ?>
                            <div class="news-content <?php echo $class; ?>">
                                <div class="top" >
                                    <span><?php $categories = get_the_category();
                                        foreach($categories as $category){
                                            if($category->category_parent==0){
                                                $link = get_category_link( $category->term_id );
                                                echo sprintf('<a class="news_cat" href="%s">%s</a>', $link, $category->cat_name);
                                            }
                                        } ?></span><span class="post-info"><?php _e('Posted', THEME_TEXT);?>: <?php echo get_the_date('d F')?></span>
                                </div>
                                <div class="bottom">
                                    <h1><?php the_title();?></h1>
                                    <?php the_excerpt();?>
                                    <a href="<?php echo get_post_permalink()?>" class="btn btn-default"><?php _e('Read more', THEME_TEXT);?></a>
                                </div>
                            </div>
                            <div style="clear: both"></div>
                        </article>
                        <?php
                        endwhile;
                        foreach ($links as $link){
                            $href_link = vc_build_link($link['news_link']);
                            printf('<a class="%s" href="%s">%s</a><br>', 'arrow', $href_link['url'], $href_link['title']);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
} // End Element Class

// Element Class Init
new WcFactsAndNews();