<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class WcComingEvents extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_comingevents_mapping' ) );
        add_shortcode( 'wc_comingevents', array( $this, 'wc_comingevents_html' ) );
    }

    // Element Mapping
    public function wc_comingevents_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __('W&Co Coming Events', 'text-domain'),
                'base' => 'wc_comingevents',
                'description' => __('List of coming events', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'event_links',
                        'group' => 'Links',
                        'heading' => __('Add Link', 'text-domain'),
                        'description' => __('Add links to the latest news section', 'text-domain'),
                        'params' => array(
                            VcElementAsset::vc_link('event_link')
                        )
                    ),
                    VcElementAsset::attach_image('ce_image', 'Image')
                )
            )
        );
    }

    // Element HTML
    public function wc_comingevents_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'ce_image' => '',
        ),
            $atts ));

        $links= vc_param_group_parse_atts( $atts['event_links'] );
        $events = EventCalendar::getEventPosts(true,3);
        ob_start();
       if($events) {
        ?>
        <div class="wc-facts-and-news">
            <div class="col-2 ce_image_height">
                <div class="latest-news">
                    <div class="news-links">
                        <h2 style="margin-top: -10px;"><?php echo __('Coming events', THEME_TEXT);?></h2>
                    <?php
                    wp_reset_postdata();
                        foreach ($events  as $event ) {
                            $class = 'full-width';
                            $eventId = $event->ID;
                            $image_url = get_the_post_thumbnail_url($eventId);
                            $start_date = get_field('event-start', $event->ID);
                            $start_date = '<div class="c-date start"><span class="c-day-month">'.EventCalendar::formatDate($start_date, 'j/n').'</span><span class="c-year">'.EventCalendar::formatDate($start_date, 'Y').'</span></div>';
                            $end_date = get_field('event-end', $event->ID);
                            $end_date = '<div class="c-date end"><span class="c-day-month">'.EventCalendar::formatDate($end_date, 'j/n').'</span><span class="c-year">'.EventCalendar::formatDate($end_date, 'Y').'</span></div>';
                            $date = $start_date.' <i class="fa fa-long-arrow-right" aria-hidden="true"></i> '.$end_date;
                            if($end_date==$start_date){
                                $date = $start_date;
                            }
                            ?>
                        <article>
                            <?php if ( $image_url ) { $class=''?> <div class="news-image" style="background-image: url(<?php echo $image_url?>);"></div><?php } ?>
                            <div class="news-content <?php echo $class; ?>">
                                <div class="top">
                                    <div class="c-date-container"><?php echo $date; ?></div>
                                </div>
                                <div class="bottom">
                                    <h1><?php echo $event->post_title; ?></h1>
                                    <?php echo get_the_excerpt($eventId);?>
                                    <a href="<?php echo get_post_permalink($eventId)?>" class="btn btn-default"><?php _e('Read more', THEME_TEXT);?></a>
                                </div>
                            </div>
                        </article>
                        <?php
                        }
                        foreach ($links as $link){
                            $href_link = vc_build_link($link['event_link']);
                            printf('<a class="%s" href="%s">%s</a><br>', 'arrow', $href_link['url'], $href_link['title']);
                        } ?>
                    </div>
                </div>
            </div>
            <div class="col-1">
                <div class="ce_image" style="background-image: url('<?php echo wp_get_attachment_url($ce_image, 'full') ?>')"></div>
            </div>
        </div>
        <?php
       }
        return ob_get_clean();
    }
} // End Element Class

// Element Class Init
new Wccomingevents();