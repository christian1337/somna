<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class wcPostWidget extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_post_widget_mapping' ) );
        add_shortcode( 'wc_post_widget', array( $this, 'wc_post_widget_html' ) );
    }

    // Element Mapping
    public function wc_post_widget_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Post widget', 'text-domain'),
                'base' => 'wc_post_widget',
                'description' => __('Post widget', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'General',
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_post_widget_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'post_type' => 'case',
            'preamble'   => '',
        ),
            $atts ));
        ob_start();
        ?>
        <?php
        $query_args = array(
            'post_type' => $post_type,
            'posts_per_page' => -1
        );

        $query = new WP_Query($query_args);
        ?>
        <?php if($query->have_posts()) : ?>
            <div class="articles-inline">
            <?php while($query->have_posts()) : $query->the_post(); ?>
                <article class="article-inline">
                    <div class="article__image">
                        <a href="<?php echo get_post_permalink()?>">
                            <?php the_post_thumbnail()?>
                        </a>
                    </div>
                    <div class="article__content">
                        <h3><?php the_title();?></h3>
                        <p><?php the_excerpt();?></p>
                        <a href="<?php echo get_post_permalink()?>" class="btn btn-default"><?php _e('Read more', THEME_TEXT);?></a>
                    </div>
                </article>
            <?php endwhile; ?>
            </div>
        <?php else : ?>

            <p><?php _e('No posts', THEME_TEXT);?></p>

        <?php endif; ?>

        <?php wp_reset_postdata(); ?>
    <?
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new wcPostWidget();