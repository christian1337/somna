<?php
/*
Element Description: Large background image with centered text header.
*/

// Element Class
class wcAreaWidget extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_area_widget_mapping' ) );
        add_shortcode( 'wc_area_widget', array( $this, 'wc_area_widget_html' ) );
    }

    // Element Mapping
    public function wc_area_widget_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        $_dropDown = [];
        $terms = get_terms( 'area_cat', array(
            'hide_empty' => false,
        ) );

        $_dropDown = array();
        $_dropDown['Choose area of use'] = '';
        foreach ( $terms as $term ) {
            $_dropDown[$term->name] = $term->slug;
        }

        /*array(
            __( 'Option 1 Label',  "my-text-domain"  ) => 'option1value',
            __( 'Option 2 Label',  "my-text-domain"  ) => 'option2value',
            __( 'Option 3 Label',  "my-text-domain"  ) => 'option3value',
        )*/

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Area of use widget', 'text-domain'),
                'base' => 'wc_area_widget',
                'description' => __('Displays area of use posts in a grid', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Header', 'text-domain' ),
                        'param_name' => 'header',
                        'value' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'General',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Area Category', 'text-domain' ),
                        'param_name' => 'area_slug',
                        'value' =>  $_dropDown,
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'General',
                    )
                )
            )
        );
    }

    // Element HTML
    public function wc_area_widget_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'post_type' => 'area',
            'header'   => '',
            'area_slug' => []
        ),
            $atts ));

        ob_start();
        ?>
        <?php

        $query_args = array(
            'post_type' => $post_type,
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'area_cat',
                    'field' => 'slug',
                    'terms' => $area_slug,
                )
            )
        );

        $query = new WP_Query($query_args);
        ?>
        <?php if($query->have_posts()) : ?>
            <section class="section">
                <header class="section__head">
                    <h3 class="section__title"><?php echo $header; ?></h3>
                </header>
                <div class="section__body">
                    <div class="featured-items">
                    <?php while($query->have_posts()) : $query->the_post(); ?>
                        <div class="featured-item">
                            <div class="featured__body">
                                <a href="<?php echo get_post_permalink()?>">
                                    <div class="featured__image">
                                        <?php the_post_thumbnail('thumbnail')?>
                                    </div><!-- /.featured__image -->

                                    <div class="featured__content">
                                        <span><?php the_title();?></span>
                                    </div><!-- /.featured__content -->
                                </a>
                            </div><!-- /.featured__body -->
                        </div><!-- /.featured-item -->
                <?php endwhile; ?>'
                    </div>
                </div>
            </section>
        <?php else : ?>
            <p><?php _e('No posts', THEME_TEXT) ?></p>
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>
    <?
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new wcAreaWidget();