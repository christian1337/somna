<?php
/*
Element Description: Button for Enzymex.
*/

// Element Class
class wcButton extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_button_mapping' ) );
        add_shortcode( 'wc_button', array( $this, 'wc_button_html' ) );
    }

    // Element Mapping
    public function wc_button_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Button', 'text-domain'),
                'base' => 'wc_button',
                'description' => __('Button', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    VcElementAsset::dropdown('txt_align', ['left', 'center', 'right'], 'Text align'),
                    VcElementAsset::vc_link(),
                )
            )
        );
    }

    // Element HTML
    public function wc_button_html( $atts, $content = null ) {
        $href = vc_build_link( $atts['url']);
        $text_align = isset($atts['txt_align']) ? $atts['txt_align'] : 'left';
        ob_start();
        ?>
        <div style="text-align: <?php echo $text_align; ?>">
            <a href="<?php echo $href['url'] ?>" target="<?php echo $href['target'] ?>" class="button large"><?php echo $href['title'] ?></a>
        </div>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new wcButton();