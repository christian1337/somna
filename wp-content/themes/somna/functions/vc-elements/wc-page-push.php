<?php
class WcPagePush extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'wc_page_push_mapping' ) );
        add_shortcode( 'wc_page_push', array( $this, 'wc_page_push_html' ) );
    }

    // Element Mapping
    public function wc_page_push_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('W&Co Page push with icon', 'text-domain'),
                'base' => 'wc_page_push',
                'description' => __('Image push with text and icon', 'text-domain'),
                'category' => __('Wallmander & Co', 'text-domain'),
                'icon' => get_template_directory_uri().'/functions/vc-elements/assets/img/wco.png',
                'params' => array(
                    VcElementAsset::attach_image('image'),
                    VcElementAsset::dropdown('icon', ['day', 'night', 'sensitive', 'private', 'pro', 'none'], 'Icon'),
                    VcElementAsset::vc_link(),
                )
            )
        );
    }

    // Element HTML
    public function wc_page_push_html( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'image' => '',
            'url'   => '',
            'icon'  => 'day',
        ),
            $atts ));

        $href = vc_build_link( $url);

        ob_start();
        ?>
        <div class="push">
                <a href="<?php echo $href['url'] ?>" target="<?php echo $href['target'] ?>">
                <div class="push-item">
                    <div class="image"><img src="<?php echo wp_get_attachment_url($image, 'full')  ?>"> </div>
                    <div class="content <?php echo Icons::icon_content_class($icon); ?>">
                        <?php if(Icons::icon($icon)) { ?>
                            <div class="icon"><?php echo Icons::icon($icon); ?></div>
                        <?php } ?>
                        <div class="title"><?php echo $href['title'] ?></div>
                    </div>
                </div>
                </a>
            <div style="clear: both"></div>
        </div>
        <?php
        return ob_get_clean();
    }

} // End Element Class

// Element Class Init
new WcPagePush();