<?php

// Sidebars & Widgetizes Areas
register_sidebar(array(
    'id' => 'sidebarblog',
    'name' => 'Blog Sidebar',
    'description' => 'Visas på bloggsidan.',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));