<?php
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

//add_filter( 'woocommerce_enqueue_styles', '__return_empty_array');

add_action( 'wp_enqueue_scripts', function() {
    if ( function_exists( 'is_woocommerce' ) ) {
        if ( is_front_page() ) {
            wp_dequeue_style( 'woocommerce_frontend_styles' );
            wp_dequeue_style( 'woocommerce-general' );
            wp_dequeue_style( 'woocommerce-layout' );

            /*wp_dequeue_style( 'wpml-legacy-dropdown-0' );
            wp_dequeue_style( 'wcml-dropdown-0-css' );
            wp_dequeue_style( 'wcml-dropdown-0' );*/
        }
    }
}, 99 );


add_filter( 'script_loader_tag', function($tag, $handle, $src){
    // The handles of the enqueued scripts we want to defer
    $defer_scripts = array(
        'theme-script',
        'wc-add-to-cart',
        'vc_woocommerce-add-to-cart-js',
        'cart-widget',
        'wcml-front-scripts',
        'jquery',
        'jquery-migrate',
        'sitepress-scripts',
        'wpml-cpi-scripts',
    );

    $ascync_scripts = array(
        'vc_youtube_iframe_api_js'
    );

    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }

    if ( in_array( $handle, $ascync_scripts ) ) {
        return '<script src="' . $src . '" async="async" type="text/javascript"></script>' . "\n";
    }

    return $tag;
}, 10, 3 );