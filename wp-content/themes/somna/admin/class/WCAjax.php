<?php
class WCAjax {
    public static function init() {
        self::_addAction('wp_ajax_admin_data', 'admin_data');
    }

    public static function admin_data(){
        $call = $_POST['call'];
        $vars = $_POST['vars'];
        switch ($call){
            case 'termsForAttribute':
                $taxonomy = 'pa_'.$vars;
                $terms = get_terms( array(
                    'taxonomy' => $taxonomy,
                    'hide_empty' => false,
                ) );
                //get_term_by('id', 12, 'category');
                echo json_encode($terms);
                break;
        }
        die;
    }

    protected static function _addAction($hook, $method, $priority = 10, $accepted_args = 1) {
        add_Action($hook, array(__CLASS__, $method), $priority, $accepted_args);
    }
}

WCAjax::init();