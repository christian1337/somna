<?php
get_header();

add_filter( 'woocommerce_show_page_title','__return_false');
?>

<div class="container">
    <?php
    $_main_product_cat = MainProductCat::getPage(true);
    if (!$_main_product_cat){
    ?>
    <div id="content-column">
        <?php woocommerce_content();?>
    </div>
    <?php } ?>
</div>
<?php
get_footer();