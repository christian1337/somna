<div class="sidebar">
    <div class="widget">
        <h3 class="widget__title"><?php _e('References', THEME_TEXT)?></h3>
        <ul>
    <?php

    $terms = get_terms( 'ref_cat', array(
        'hide_empty' => false,
        'orderby' => 'menu_order'
    ) );
    ?>
            <ul>
    <?php
    foreach ( $terms as $term ) {
        $class='cat';
        $tax_slug = get_queried_object()->slug;
        if($tax_slug==$term->slug){
            $class='cat current-cat';
        }
        echo '<li class="'.$class.'"><a href="'.get_home_url().'/ref_cat/'.$term->slug.'" >' . $term->name . '</a></li>';
    }
    ?>
        </ul>
    </div>
</div>