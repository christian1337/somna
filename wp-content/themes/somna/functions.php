<?php
define('THEME_TEXT', 'somna');

require_once 'functions/core/cleanup.php';
require_once 'functions/class/WordPressAsset.php';
require_once 'functions/class/Ajax.php';
require_once 'functions/class/TaxanomyOrder.php';
require_once 'functions/class/AttributeSorting.php';
require_once 'functions/class/Icons.php';
require_once 'functions/class/Encoding.php';
require_once 'functions/class/SimpleCalendar.php';
require_once 'functions/class/EventCalendar.php';
require_once 'functions/woocomerce/extend-product-tab.php';
require_once 'functions/woocomerce/single-product.php';
require_once 'functions/woocomerce/main-product-cat.php';


//require_once 'functions/includes/post-types.php';

//require_once 'functions/class/extend-recent-post-widget.php';

// Enqueue scripts & styles
add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/css/style.css', null, '2.0.1' );
   // wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/js/theme.min.js', null, '1.4.1', false );
    wp_localize_script( 'theme-script', 'data_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )));
}, 20);

// Init
add_action( 'init', function() {
    add_filter( 'jpeg_quality', create_function( '', 'return 95;' ) );

    add_theme_support( 'post-thumbnails' );

    register_nav_menus(
        array(
            'main-menu' => __('Main Menu', THEME_TEXT),
            'footer-menu' => __('Footer Menu', THEME_TEXT),
        )
    );
    $args = array(
        'public' => true,
        'show_ui' => true,
        'label'  => __('Category Page', THEME_TEXT),
        'supports' => array( ),
        'menu_position' => 5,
    );
    register_post_type( 'main_product_cat', $args );

    $args = array(
        'public' => true,
        'show_ui' => true,
        'label'  => __('References', THEME_TEXT),
        'supports' => array( ),
        'menu_position' => 6,
    );
    register_post_type( 'references', $args );

    register_taxonomy(
        'ref_cat',
        'references',
        array(
            'label' => __( 'Categories' ),
            'rewrite' => array( 'slug' => 'ref_cat' ),
            'hierarchical' => true,
        )
    );

    function wpdocs_custom_excerpt_length( $length ) {
        return 20;
    }
    add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

    function wpdocs_excerpt_more( $more ) {
        return ' [...]';
    }
    add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );
});

// Before VC Init
add_action( 'vc_before_init', function(){
    /*require_once 'functions/vc-elements/page-intro.php';
    require_once 'functions/vc-elements/logo-grid.php';
    require_once 'functions/vc-elements/article.php';
    require_once 'functions/vc-elements/button.php';
    require_once 'functions/vc-elements/call-out.php';
    require_once 'functions/vc-elements/post-widget.php';
    require_once 'functions/vc-elements/area-widget.php';
    require_once 'functions/vc-elements/featured-content.php';
    require_once 'functions/vc-elements/contact.php';*/

    require_once 'functions/class/VcElementAsset.php';
    require_once 'functions/vc-elements/wc-bootstrap-full-width.php';
    require_once 'functions/vc-elements/wc-page-intro.php';
    require_once 'functions/vc-elements/wc-page-push.php';
    require_once 'functions/vc-elements/wc-page-push-regular.php';
    require_once 'functions/vc-elements/wc-facts-and-news.php';
    require_once 'functions/vc-elements/wc-coming-events.php';
    require_once 'functions/vc-elements/wc-key-word-table.php';
    require_once 'functions/vc-elements/button.php';
    require_once 'functions/vc-elements/article.php';
    require_once 'functions/vc-elements/wc-product-category-page.php';
    require_once 'functions/vc-elements/wc-download-table.php';
    //require_once 'functions/vc-elements/wc-product-category-push.php';
});

add_action('widgets_init', function () {
    register_sidebar([
        'name' => __('Sidebar', THEME_TEXT),
        'id' => 'sidebar',
        'before_widget' => '<aside class="widget %1$s %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget__title">',
        'after_title' => '</h3>'
    ]);
});

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter('add_to_cart_fragments', function ( $fragments ) {
    ob_start();
    ?>
    <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
        <div class="count"><div class="counter"><?php echo WC()->cart->get_cart_contents_count(); ?></div></div>
    </a>
    <?php
    $fragments['a.cart-contents .count'] = ob_get_clean();
    return $fragments;
});

// After theme setup
add_action( 'after_setup_theme', function() {
    load_theme_textdomain(THEME_TEXT, get_template_directory() . '/languages/');

    // Woocommerce support
    add_theme_support( 'woocommerce' );

    // ACF
    // Removes ACF from wp-admin
    //define( 'ACF_LITE', true );

    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page();
    }

    if( function_exists('acf_add_options_sub_page') ) {
        acf_add_options_sub_page('Settings');
        acf_add_options_sub_page('Header');
        acf_add_options_sub_page('Social');
        acf_add_options_sub_page('Footer');
    }
    require_once 'functions/acf.php';

    //add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
});

/*function wc_empty_cart_redirect_url() {
    return $_SERVER['HTTP_REFERER'];
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );*/

// Visual Comnposer admin css fix for selects
add_action('admin_head', function(){
    echo '<style>.mce-panel.mce-menu,.mce-floatpanel.mce-popover.mce-bottom.mce-start{z-index:100012!important;}</style>';
});

// Load more then 30 variations in variation form
add_filter( 'woocommerce_ajax_variation_threshold', function ( $qty, $product ) {
    return 99999;
}, 10, 2 );

// To get the order total to match between WooCommerce and Klarna you need to configure WooCommerce to display prices with 2 decimals.
// Removes decimal zeros.
add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

/**
 * Change price format from range to "From:"
 *
 * @param float $price
 * @param obj $product
 *
 * @return str
 */
function iconic_variable_price_format( $price, $product ){

	$prefix = sprintf( '%s ', __( 'From', THEME_TEXT ) );

	$min_price_regular = $product->get_variation_regular_price( 'min', true );
	$min_price_sale    = $product->get_variation_sale_price( 'min', true );
	$max_price         = $product->get_variation_price( 'max', true );
	$min_price         = $product->get_variation_price( 'min', true );

	$price = ( $min_price_sale == $min_price_regular ) ?
		wc_price( $min_price_regular ) :
		'<del>' . wc_price( $min_price_regular ) . '</del>' . '<ins>' . wc_price( $min_price_sale ) . '</ins>';

	return ( $min_price == $max_price ) ?
		$price :
		sprintf( '%s%s', $prefix, $price );

}

add_filter( 'woocommerce_variable_sale_price_html', 'iconic_variable_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'iconic_variable_price_format', 10, 2 );

add_action( 'woocommerce_proceed_to_checkout', function() {
    ?>
    <div class="newsletter2">
        <span><?php _e('Signup on our newsletter to get information & offers from Somna AB', THEME_TEXT)?></span>
        <!-- Begin MailChimp Signup Form -->
        <div id="mc_embed_signup">
            <form action="//somna.us11.list-manage.com/subscribe/post?u=7da18a37e799cfbfefb751b3d&amp;id=107f2c56e1" method="post" id="mc-embedded-subscribe-form2" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <label for="mce-EMAIL"></label>
                    <input type="email" value="" name="EMAIL" class="input-text" id="mce-EMAIL" placeholder="<?php _e('E-mail address', THEME_TEXT)?>" required>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_7da18a37e799cfbfefb751b3d_107f2c56e1" tabindex="-1" value=""></div>
                    <input type="submit" value="<?php _e('Send', THEME_TEXT);?>" name="subscribe" id="mc-embedded-subscribe" class="button-join">
                </div>
            </form>
        </div>
        <!--End mc_embed_signup-->
    </div>
    <?php
}, 9 );