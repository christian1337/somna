<?php
    get_header();
?>
<div class="container">

    <h3><?php _e('Search Results for:', THEME_TEXT)?> <?php the_search_query(); ?></h3>

	<?php if (have_posts()) : while (have_posts()) : the_post(); $pcc++;?>
        <article class="article">
            <h1><?php the_title(); ?></h1>
            <p><?php the_excerpt(); ?></p>
            <div>
                <a href="<?php the_permalink(); ?>" class="btn btn-default"><?php _e('Read more', THEME_TEXT)?></a>
            </div>
        </article>
	<?php endwhile; else :   ?>
        <p><?php _e('No posts found', THEME_TEXT)?></p>
    <?php endif;   ?>
    <hr style="margin-bottom: 10px">
	<div class="oldernewer">
		<div class="older link link--large"><?php next_posts_link('<i class="fa fa-chevron-left" aria-hidden="true"></i><i class="fa fa-chevron-left" aria-hidden="true"></i> '._x('Previous page', THEME_TEXT)) ?></div>
		<div class="newer link link--large"><?php previous_posts_link(_x('Next page', THEME_TEXT).' <i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i>') ?></div>
        <div style="clear: both"></div>
	</div>
    <hr style="margin-top: 10px">
    <!--<h3 style="text-align: center"><?php _e('Search again', THEME_TEXT)?></h3>
    <div class="blog-search search">
        <?php //get_search_form(); ?>
    </div>-->
	
</div><!-- #content -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>