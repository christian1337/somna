<?php
    $post_type = get_post_type();
    if ($post_type=='post'){
        get_template_part('archive');
        die;
    }
    get_header();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <?php get_template_part('content', get_post_format()); ?>

<?php endwhile; ?>

<?php else : ?>

    <?php get_template_part('content', 'none'); ?>

<?php endif; ?>


<?php get_footer(); ?>