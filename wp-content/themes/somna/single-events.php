<?php
    get_header();
    $post_type = get_post_type();
?>
<div class="container">
    <div class="row">
    <div class="articles">
    <?php
    if ( have_posts()){
        while ( have_posts() ) : the_post();
        ?>
            <div class="content">
                <h1><?php the_title()?></h1>
                <div class="meta">Publicerat <?php the_date();?></div>
                <p><?php the_content();?></p>
            </div>
        <?php
        endwhile;
    }
    ?>
    </div>
        <?php echo EventCalendar::getComingEvents(); ?>
    </div>
</div>
<?php get_footer();?>